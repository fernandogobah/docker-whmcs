FROM debian:stable-slim

LABEL org.opencontainers.image.authors="fernandogobah@gmail.com"

ARG TARGETARCH

ARG PHP_RELEASE

ENV TZ="UTC"

ENV PHP_VERSION=${PHP_RELEASE}

ENV APACHE_RUN_USER="www-data"

ENV APACHE_RUN_GROUP="www-data"

ENV APACHE_LOG_DIR="/var/log/apache2"

ENV APACHE_RUN_DIR="/var/run/apache2"

ENV APACHE_PID_FILE="/var/run/apache2/apache2.pid"

ENV DEBIAN_FRONTEND="noninteractive"

#ENV WHMCS_SERVER_IP="\$server_addr" WHMCS_SERVER_URL="_"

##Create structure directory
RUN bash -c 'mkdir -pv \
    /config/{apache2,php,supervisor,www} \
    /run/apache2 /run/php' && \
    mv /etc/cron.d /config && \
    ln -s /config/apache2 /etc && \
    ln -s /config/cron.d /etc && \
    ln -s /config/php /etc && \
    ln -s /config/supervisor /etc && \
    ln -s /config/www /var && \
    touch /run/php/php${PHP_VERSION}-fpm.pid

## Add repository
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    apt-transport-https ca-certificates supervisor \
    apt-utils jq cron curl unzip gpg lsb-release

RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list && \
    curl -fsSL  https://packages.sury.org/php/apt.gpg| gpg --dearmor -o /etc/apt/trusted.gpg.d/sury-keyring.gpg

#Install packages necessary
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    apache2 apache2-utils php-pear \
    php${PHP_VERSION} \
    php${PHP_VERSION}-common \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-bcmath \
    php${PHP_VERSION}-mysql \
    php${PHP_VERSION}-intl \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-soap \
    php${PHP_VERSION}-imagick \
    php${PHP_VERSION}-igbinary \
    php${PHP_VERSION}-redis \
    php${PHP_VERSION}-opcache \
    php${PHP_VERSION}-enchant \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-imap \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-xmlrpc \
    php${PHP_VERSION}-zip \
    php${PHP_VERSION}-bz2 \
    php${PHP_VERSION}-curl

#Clean after install packages
RUN apt-get -y autoremove && \
    apt-get -y purge && \
    apt-get -y clean

RUN rm -rf \
       /tmp/* \
       /var/lib/apt/lists/* \
       /var/tmp/* && \
    rm /var/log/lastlog \
       /var/log/faillog && \
    rm /etc/apache2/sites-available/* \
       /etc/apache2/sites-enabled/*

#Setup ionCube for PHP
RUN case ${TARGETARCH} in \
         "amd64")  IONCUBE_ARCH="x86-64"  ;; \
         "arm64")  IONCUBE_ARCH="aarch64" ;; \
    esac && \
    echo "**** Installing ionCube for PHP: Architecture: ${IONCUBE_ARCH} ****" && \
    mkdir /tmp/ioncube && cd /tmp/ioncube && \
    curl --user-agent "Mozilla" -o ioncube.zip http://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_${IONCUBE_ARCH}.zip && \
    unzip -q ioncube.zip && mkdir -p /usr/lib/php/ioncube && cp -vf ioncube/ioncube_loader_lin_${PHP_VERSION}.so /usr/lib/php/ioncube/ && \
    echo "zend_extension = /usr/lib/php/ioncube/ioncube_loader_lin_${PHP_VERSION}.so" > /etc/php/${PHP_VERSION}/mods-available/00-ioncube.ini && \
    ln -sf /etc/php/${PHP_VERSION}/mods-available/00-ioncube.ini /etc/php/${PHP_VERSION}/fpm/conf.d/00-ioncube.ini && \
    ln -sf /etc/php/${PHP_VERSION}/mods-available/00-ioncube.ini /etc/php/${PHP_VERSION}/cli/conf.d/00-ioncube.ini && \
    rm -rf /tmp/ioncube

## Setup SourceGuardian for PHP
##RUN case ${TARGETARCH} in \
##         "amd64")  SOURCEGUARDIAN_ARCH="x86_64"  ;; \
##         "arm64")  SOURCEGUARDIAN_ARCH="aarch64" ;; \
##    esac && \
##    echo "**** Installing SourceGuardian for PHP: Architecture: ${SOURCEGUARDIAN_ARCH} ****" && \
##    mkdir /tmp/sourceguardian && cd /tmp/sourceguardian && \
##    curl --user-agent "Mozilla" -o sourceguardian.zip https://www.sourceguardian.com/loaders/download/loaders.linux-${SOURCEGUARDIAN_ARCH}.zip && \
##    unzip -q sourceguardian.zip && mkdir -p /usr/lib/php/sourceguardian && cp -vf ixed.${PHP_VERSION}.lin /usr/lib/php/sourceguardian/ && \
##    echo "zend_extension=/usr/lib/php/sourceguardian/ixed.${PHP_VERSION}.lin" > /etc/php/${PHP_VERSION}/mods-available/00-sourceguardian.ini && \
##    ln -sf /etc/php/${PHP_VERSION}/mods-available/00-sourceguardian.ini /etc/php/${PHP_VERSION}/fpm/conf.d/00-sourceguardian.ini && \
##    ln -sf /etc/php/${PHP_VERSION}/mods-available/00-sourceguardian.ini /etc/php/${PHP_VERSION}/cli/conf.d/00-sourceguardian.ini && \
##    rm -rf /tmp/sourceguardian
#

#Setup WHMCS
RUN echo "**** Setting WHMCS Release Version ****" && \
    if [ "x${WHMCS_RELEASE}" = "x" ]; then \
        WHMCS_RELEASE=$(curl -sX GET 'https://api1.whmcs.com/download/latest?type=release' \
        | jq -r '.version'); \
    fi && \
    echo "**** Downloading WHMCS Release: ${WHMCS_RELEASE} ****" && \
    curl --user-agent "Mozilla" -o /var/www/whmcs.zip -L \
        https://releases.whmcs.com/v2/pkgs/whmcs-${WHMCS_RELEASE}-release.1.zip && \
    unzip /var/www/whmcs.zip -d /var/www/whmcs && \
    chown -R www-data. /var/www && rm /var/www/whmcs.zip

#Copy config files
COPY supervisord.conf /config/supervisor/supervisord.conf

COPY whmcs.conf /config/apache2/sites-available/whmcs.conf

#Enable permissions, configs, modules and website 
RUN chown -R www-data:www-data /config/www && \
    a2enconf php${PHP_VERSION}-fpm && \
    a2enmod alias proxy proxy_fcgi setenvif rewrite && \
    a2ensite whmcs

#Create volumes
VOLUME /config

WORKDIR /config

#Expose port access
EXPOSE 80

#Start daemons
CMD ["/usr/bin/supervisord"]
